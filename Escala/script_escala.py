from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

separacion = 20                                                             # Separacion entre marcas
alto = 39
offset = 1
ancho = separacion * 639 + offset                                           # Son 639 porque el intervalo del angulo es [0, 6400)

size = (ancho, alto)

imgNueva = Image.new(mode="RGBA", size=size, color=(255,255,255,0))
font = ImageFont.truetype("Escala\OpenSans.ttf", 18)
draw = ImageDraw.Draw(imgNueva)

sombreado = (0, 0, 0, 80)
negro = (0, 0, 0, 255)
rojo = (255, 0, 0, 255)

color = rojo

for x in range (4, size[0], separacion):
    if(x % 100 != 4):                                                       # Esta guarda es para colocar las marcas "normales"
        for y in range (38, 26, -1):
            imgNueva.putpixel((x, y), color)
            imgNueva.putpixel((x+1, y), sombreado)
    elif(x % 200 == 4):                                                     # Esta guarda es para colocar las guardas que llevan numero las cuales son las mas altas
        for y in range (38, 14, -1):
            imgNueva.putpixel((x, y), color)
            imgNueva.putpixel((x+1, y), sombreado)
        numero = (x // 200) * 100
        if(numero < 1000 and numero != 0):                                  # El 0 lo excluyo porque es un caso especial
            draw.text((x-15, -6), str(numero), (255, 0, 0), font=font)      # Las restas son para centrar el texto arriba de la marca
        elif(numero >= 1000 and numero != 0):                               # Para los numeros de 4 cifras necesito usar un offset mas grande para centrarlos
            draw.text((x-20, -6), str(numero), (255, 0, 0), font=font)
        draw.text((0, -6), str(0), (255, 0, 0), font=font)
    else:                                                                   # Esta guarda coloca las marcas del medio
        for y in range (38, 19, -1):
            imgNueva.putpixel((x, y), color)
            imgNueva.putpixel((x+1, y), sombreado)

imgNueva.save("Escala\escala_nueva_noche.png")
